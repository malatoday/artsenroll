window.onload = function(){
    function getUrl(p){
        return p;
    }
    var lastweekbarChartData = {
        labels : [],
        datasets : [
            {
                fillColor : "rgba(220,0,0,0.5)",
                strokeColor : "rgba(220,0,0,0.8)",
                highlightFill: "rgba(220,0,0,0.75)",
                highlightStroke: "rgba(220,0,0,1)",
                data : []
            },
            {
                fillColor : "rgba(0,220,0,0.5)",
                strokeColor : "rgba(0,220,0,0.8)",
                highlightFill: "rgba(0,220,0,0.75)",
                highlightStroke: "rgba(0,220,0,1)",
                data : []
            }
        ]
    };
    var lastweekpayedbarChartData = {
            labels:[],
            datasets:[
                {
                    fillColor:"rgba(0,220,0,0.5)",
                    strokeColor : "rgba(0,220,0,0.8)",
                    highlightFill: "rgba(0,220,0,0.75)",
                    highlightStroke: "rgba(0,220,0,1)",
                    data : []
                }
            ]
    };
    var lastweekcanvas = document.getElementById("lastweekcanvas");
    var lastweekpayedcanvas = document.getElementById("lastweekpayedcanvas");
    var lastweekcanvasctx = lastweekcanvas.getContext("2d");
    var lastweekpayedcanvasctx = lastweekpayedcanvas.getContext("2d");
    $.post(getUrl("getWeekInfo"),{}, function(data, state){
        var i,labels = [],mdata=[],mdata2=[];
        for(i = 0; i<7; i++){
            labels.push(data[i].name);
            mdata.push(data[i].numofnotpayed);
            mdata2.push(data[i].numofpayed);
        }
        lastweekbarChartData.labels = labels;
        lastweekbarChartData.datasets[0].data = mdata;
        lastweekbarChartData.datasets[1].data = mdata2;
        window.lastweekBar = new Chart(lastweekcanvasctx).Bar(lastweekbarChartData, {
            responsive : true
        });
    });
    $.post(getUrl("getWeekPayedInfo"),{},function(data, state){
        var i,labels = [],mdata=[];
        for(i = 0; i<7; i++){
            labels.push(data[i].name);
            mdata.push(data[i].num);
        }
        lastweekpayedbarChartData.labels = labels;
        lastweekpayedbarChartData.datasets[0].data = mdata;
        window.lastweekpayedBar = new Chart(lastweekpayedcanvasctx).Bar(lastweekpayedbarChartData, {
            responsive : true
        });
    });
    lastweekcanvas.onclick = function(evt){
        var activeBars = window.lastweekBar.getBarsAtEvent(evt);
        console.log(activeBars);
    }
    // 各考点报名情况
    var examcenterenrollChartData = {
        labels : [],
        datasets : [
            {
                fillColor : "rgba(220,0,0,0.5)",
                strokeColor : "rgba(220,0,0,0.8)",
                highlightFill: "rgba(220,0,0,0.75)",
                highlightStroke: "rgba(220,0,0,1)",
                data : []
            },
            {
                fillColor : "rgba(0,220,0,0.5)",
                strokeColor : "rgba(0,220,0,0.8)",
                highlightFill: "rgba(0,220,0,0.75)",
                highlightStroke: "rgba(0,220,0,1)",
                data : []
            }
        ]
    };
    var examcenterenrollcanvas = document.getElementById('examcenterenrollcanvas');
    var examcenterenrollctx = examcenterenrollcanvas.getContext('2d');
    var table = $('#examcenterenrolltable');
    $.post(getUrl('getExamcenterEnrolls'), {},function(data, state){
        var i,labels = [],mdata=[],mdata2=[];
        var trs = '<tr>',tre = '</tr>', tds = '<td>', tde = '</td>';
        console.log(data);
        for(i = 0; i<data.length; i++){
            labels.push(data[i].name);
            mdata.push(data[i].enrollnum);
            mdata2.push(data[i].payednum);
            table.append(trs + tds + data[i].name + tde + tds + data[i].enrollnum + tde + tds + data[i].payednum + tde + tds + '<a href="export?id=' + data[i].id +'">导出为excel</a>' + tde + tre);
        }
        examcenterenrollChartData.labels = labels;
        examcenterenrollChartData.datasets[0].data = mdata;
        examcenterenrollChartData.datasets[1].data = mdata2;
        window.examcenterenrollBar = new Chart(examcenterenrollctx).Bar(examcenterenrollChartData, {
            responsive : true
        });
    });

}
