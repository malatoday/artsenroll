-- MySQL dump 10.13  Distrib 5.6.25, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: artsenroll
-- ------------------------------------------------------
-- Server version	5.6.25-0ubuntu0.15.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `art_access`
--

DROP TABLE IF EXISTS `art_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `art_access` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) CHARACTER SET utf8 NOT NULL,
  `pid` int(8) NOT NULL DEFAULT '0',
  `level` int(8) NOT NULL,
  `title` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `art_access`
--



--
-- Table structure for table `art_accessrole`
--

DROP TABLE IF EXISTS `art_accessrole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `art_accessrole` (
  `role_id` int(8) NOT NULL,
  `access_id` int(8) NOT NULL,
  `uptime` int(20) NOT NULL,
  PRIMARY KEY (`role_id`,`access_id`),
  KEY `access_id` (`access_id`),
  CONSTRAINT `art_accessrole_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `art_role` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `art_accessrole_ibfk_2` FOREIGN KEY (`access_id`) REFERENCES `art_access` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `art_accessrole`
--



--
-- Table structure for table `art_enroll`
--

DROP TABLE IF EXISTS `art_enroll`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `art_enroll` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `uid` int(8) NOT NULL,
  `enrolltime` int(20) NOT NULL,
  `uptime` int(20) NOT NULL,
  `ispayed` int(8) NOT NULL,
  `major` varchar(45) NOT NULL,
  `examcenter` int(8) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `art_enroll`
--



--
-- Table structure for table `art_examcenter`
--

DROP TABLE IF EXISTS `art_examcenter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `art_examcenter` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `fromprovince` varchar(45) NOT NULL,
  `forprovince` varchar(120) NOT NULL,
  `inuse` int(8) NOT NULL DEFAULT '0',
  `majors` varchar(120) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `art_examcenter`
--



--
-- Table structure for table `art_ip`
--

DROP TABLE IF EXISTS `art_ip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `art_ip` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) CHARACTER SET utf8 NOT NULL,
  `title` varchar(45) CHARACTER SET utf8 DEFAULT '',
  `isopen` int(8) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `art_ip`
--

--
-- Table structure for table `art_iptouser`
--

DROP TABLE IF EXISTS `art_iptouser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `art_iptouser` (
  `uid` int(8) NOT NULL,
  `ip` int(8) NOT NULL,
  `uptime` int(20) NOT NULL,
  PRIMARY KEY (`uid`,`ip`),
  KEY `ip` (`ip`),
  CONSTRAINT `art_iptouser_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `art_user` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `art_iptouser_ibfk_2` FOREIGN KEY (`ip`) REFERENCES `art_ip` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `art_iptouser`
--


--
-- Table structure for table `art_logip`
--

DROP TABLE IF EXISTS `art_logip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `art_logip` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `ip` varchar(45) CHARACTER SET utf8 NOT NULL,
  `occurtime` int(20) NOT NULL,
  `username` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `isok` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `art_logip`
--

--
-- Table structure for table `art_major`
--

DROP TABLE IF EXISTS `art_major`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `art_major` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `level` int(8) NOT NULL DEFAULT '0',
  `pid` int(8) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `art_major`
--

--
-- Table structure for table `art_project`
--

DROP TABLE IF EXISTS `art_project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `art_project` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `examcenter` varchar(45) NOT NULL DEFAULT '0',
  `major` varchar(45) NOT NULL DEFAULT '0',
  `title` varchar(120) NOT NULL DEFAULT '',
  `uptime` int(20) NOT NULL,
  `isopen` int(8) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `art_project`
--


--
-- Table structure for table `art_role`
--

DROP TABLE IF EXISTS `art_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `art_role` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `name_2` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `art_role`
--

--
-- Table structure for table `art_user`
--

DROP TABLE IF EXISTS `art_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `art_user` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `role_id` int(8) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `art_user`
--

LOCK TABLES `art_user` WRITE;
/*!40000 ALTER TABLE `art_user` DISABLE KEYS */;
INSERT INTO `art_user` VALUES (1,'admin','59acedf8dbbe510ff69cb6315deada42','闫兴茂',1);
/*!40000 ALTER TABLE `art_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `art_userinfo`
--

DROP TABLE IF EXISTS `art_userinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `art_userinfo` (
  `uid` int(8) NOT NULL,
  `province` int(8) NOT NULL,
  `stunumber` varchar(45) NOT NULL,
  `address` varchar(45) NOT NULL,
  `zipcode` int(8) NOT NULL,
  `telofparent` varchar(45) NOT NULL,
  `tel` varchar(20) NOT NULL,
  `sex` int(8) NOT NULL,
  `birthday` varchar(45) DEFAULT NULL,
  `politics` int(8) DEFAULT NULL,
  `isfresh` int(8) DEFAULT NULL,
  `nation` int(8) DEFAULT NULL,
  `school` varchar(45) DEFAULT NULL,
  `qq` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `stunumber` (`stunumber`),
  UNIQUE KEY `stunumber_2` (`stunumber`),
  CONSTRAINT `art_userinfo_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `art_user` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `art_userinfo`
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-28 16:04:20
