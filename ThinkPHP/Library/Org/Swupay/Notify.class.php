<?php
namespace Org\Swupay;

class Notify {
    private $orderDate; // 交易日期时间
    private $orderNo;   // 订单号
    private $amount;    // 订单金额
    private $jylsh;     // 支付平台交易流水号
    private $tranStat;  // 订单支付状态
    private $return_type;   // 通知类型
    private $sign;  // 订单签名数据
    private $key;   // 签名秘钥

    private $query_string;

    public function __construct($notify_type = 'get'){
        if($notify_type == 'get'){
            $this->init_from_get();
        }else{
            $this->init_from_post();
        }
    }

    public function checkSign(){
        if(md5($this->get_query_string() == $this->sign)){
            // 校验成功
            return [
                'orderDate' =>  $this->orderDate,
                'orderNo'   =>  $this->orderNo,
                'amount'    =>  $this->amount,
                'jylsh'     =>  $this->jylsh,
                'tranStat'  =>  $this->tranStat,
                'return_type'   =>  $this->return_type
            ];
        }else{
            return false;
        }
    }

    private function get_query_string(){
        if(empty($this->query_string)){
            $this->query_string = 'orderDate=' . $this->orderDate .
                                    '&orderNo=' . $this->orderNo .
                                    '&amount=' . $this->amount .
                                    '&jylsh=' . $this->jylsh .
                                    '&tranStat=' . $this->tranStat .
                                    '&return_type=' . $this->return_type;
        }
        return $this->query_string;
    }

    private function init_from_get(){
        $this->orderDate = $_GET['orderDate'];
        $this->orderNo = $_GET['orderNo'];
        $this->amount = $_GET['amount'];
        $this->jylsh = $_GET['jylsh'];
        $this->tranStat = $_GET['tranStat'];    // 此处应该是1
        $this->return_type = $_GET['return_type'];  // 此处应该是1
        $this->sign = $_GET['sign'];
    }

    private function init_from_post(){
        $this->orderDate = $_POST['orderDate'];
        $this->orderNo = $_POST['orderNo'];
        $this->amount = $_POST['amount'];
        $this->jylsh = $_POST['jylsh'];
        $this->tranStat = $_POST['tranStat'];    // 此处应该是1
        $this->return_type = $_POST['return_type'];  // 此处应该是2
        $this->sign = $_POST['sign'];
    }
}
?>
