<?php
namespace Org\Swupay;

class Pay {
    private $orderDate;  // 交易时间yyyyMMddHHmmss
    private $orderNo;    // 订单号 String(20)
    private $amount;     // 订单金额，String(18)，范围[0.01-100000000.00],精确到小数点后两位，不可为0
    private $xmpch;      // 支付平台项目批次号。对接之前在支付平台申请的收费项目编号，收费批次号，格式为sfxmbh-sfpch
    private $return_url; // 页面跳转同步通知url
    private $notify_url; // 服务器异步通知页面路径
    private $query_string;// 请求字符串数据
    private $sign;       // 订单签名数
    private $key;        // 加密秘钥
    private $url;
    const PRODUCT_KEY = '1drYnNs5I25bVLreKZ363yofuOC4z4jS';
    const PRODUCT_XMPCH = '004-2016010003';

    public function __construct($orderNo, $amount){
        date_default_timezone_set('PRC');
        $this->orderNo = $orderNo;
        $this->amount = $amount;
        $this->orderDate = date('YmdHis');
        $this->setDebug(true);  // 默认开启debug模式
    }

    public function send(){
        if($this->sign()){
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $this->url . '?' . $this->query_string . '&sign=' . $this->sign);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_exec($ch);
            curl_close($ch);
        }
    }

    public function setRetrunUrl($url){
        $this->return_url = $url;
    }

    public function setNotifyUrl($url){
        $this->notify_url = $url;
    }

    public function setDebug($isdebug = true){
        if($isdebug){
            $this->url = 'http://221.238.143.40/zhifu/payAccept.aspx';
            $this->key = 'umz4aea6g97skeect0jtxigvjkrimd0o';
            $this->xmpch = '004-2014050001';
        }else{
            $this->url = 'http://upay.swu.edu.cn/zhifu/payAccept.aspx';
            $this->key = self::PRODUCT_KEY;
            $this->xmpch = self::PRODUCT_XMPCH;
        }
    }

    /**
     * 生成签名
    */
    public function sign(){
        if($this->convert_query_string()==false)return false;
        $this->sign = md5($this->query_string . $this->key);
        return true;
    }

    /**
     * 组装请求数据，不包含sign
    */
    private function convert_query_string(){
        if(empty($this->orderDate) || empty($this->orderNo) || empty($this->amount) || empty($this->xmpch) || empty($this->return_url) || empty($this->notify_url)){
            return false;
        }else{
            $this->query_string = 'orderDate=' . $this->orderDate .
             '&orderNo=' . $this->orderNo .
             '&amount=' . $this->amount .
             '&xmpch=' . $this->xmpch .
             '&return_url=' . $this->return_url .
             '&notify_url=' . $this->notify_url;
             return true;
         }
    }

    public function get_query_string(){
        return $this->query_string;
    }

    public function get_query_array(){
        return [
            'orderDate'=>$this->orderDate,
            'orderNo'=>$this->orderNo,
            'amount'=>$this->amount,
            'xmpch'=>$this->xmpch,
            'return_url'=>$this->return_url,
            'notify_url'=>$this->notify_url,
            'sign'=>$this->sign,
        ];
    }

    public function get_url(){
        return $this->url;
    }
}
