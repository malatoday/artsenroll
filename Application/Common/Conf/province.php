<?php 
return array(
	'PROVINCE'	=>	array(
		'BEIJING'	=>	array(
			'code'	=>	110000,
			'name'	=>	'北京市'
		),
		'TIANJIN'	=>	array(
			'code'	=>	120000,
			'name'	=>	'天津市'
		),
		'HEBEI'		=>	array(
			'code'	=>	130000,
			'name'	=>	'河北省'
		),
		'SHANXI'	=>	array(
			'code'	=>	140000,
			'name'	=>	'山西省'
		),
		'NEIMENGGU'	=>	array(
			'code'	=>	150000,
			'name'	=>	'内蒙古自治区'
		),
		'LIAONING'	=>	array(
			'code'	=>	210000,
			'name'	=>	'辽宁省'
		),
		'JILING'	=>	array(
			'code'	=>	220000,
			'name'	=>	'吉林省'
		),
		'HEILONGJIANG'	=>	array(
			'code'	=>	230000,
			'name'	=>	'黑龙江省'
		),
		'SHANGHAI'	=>	array(
			'code'	=>	310000,
			'name'	=>	'上海市'
		),
		'JIANGSU'	=>	array(
			'code'	=>	320000,
			'name'	=>	'江苏省'
		),
		'ZHEJIANG'	=>	array(
			'code'	=>	330000,
			'name'	=>	'浙江省'
		),
		'ANHUI'		=>	array(
			'code'	=>	340000,
			'name'	=>	'安徽省'
		),
		'FUJIAN'	=>	array(
			'code'	=>	350000,
			'name'	=>	'福建省'
		),
		'JIANGXI'	=>	array(
			'code'	=>	360000,
			'name'	=>	'江西省'
		),
		'SHANDONG'	=>	array(
			'code'	=>	370000,
			'name'	=>	'山东省'
		),
		'HENAN'		=>	array(
			'code'	=>	410000,
			'name'	=>	'河南省'
		),
		'HUBEI'		=>	array(
			'code'	=>	420000,
			'name'	=>	'湖北省'
		),
		'HUNAN'		=>	array(
			'code'	=>	430000,
			'name'	=>	'湖南省'
		),
		'GUANGDONG'	=>	array(
			'code'	=>	440000,
			'name'	=>	'广东省'
		),
		'GUANGXI'	=>	array(
			'code'	=>	450000,
			'name'	=>	'广西壮族自治区'
		),
		'HAINAN'	=>	array(
			'code'	=>	460000,
			'name'	=>	'海南省'
		),
		'CHONGQING'	=>	array(
			'code'	=>	500000,
			'name'	=>	'重庆市'
		),
		'SICHUAN'	=>	array(
			'code'	=>	510000,
			'name'	=>	'四川省'
		),
		'GUIZHOU'	=>	array(
			'code'	=>	520000,
			'name'	=>	'贵州省'
		),
		'YUNNAN'	=>	array(
			'code'	=>	530000,
			'name'	=>	'云南省'
		),
		'XIZANG'	=>	array(
			'code'	=>	540000,
			'name'	=>	'西藏自治区'
		),
		'SHANXIS'	=>	array(
			'code'	=>	610000,
			'name'	=>	'陕西省'
		),
		'GANSU'		=>	array(
			'code'	=>	620000,
			'name'	=>	'甘肃省'
		),
		'QINGHAI'	=>	array(
			'code'	=>	630000,
			'name'	=>	'青海省'
		),
		'NINGXIA'	=>	array(
			'code'	=>	640000,
			'name'	=>	'宁夏回族自治区'
		),
		'XINJIANG'	=>	array(
			'code'	=>	650000,
			'name'	=>	'新疆维吾尔自治区'
		),
		'TAIWAN'	=>	array(
			'code'	=>	710000,
			'name'	=>	'台湾省'
		),
		'XIANGGANG'	=>	array(
			'code'	=>	810000,
			'name'	=>	'香港特别行政区'
		),
		'AOMEN'	=>	array(
			'code'	=>	820000,
			'name'	=>	'澳门特别行政区'
		),
	)
);