<?php
	function isLogin(){
		if(empty($_SESSION) || session('rid')!=0){
			return false;
		}
		return true;
	}

	function getUserinfo(){
		return array(
			'name'	=>	session('name'),
			'username'	=>	session('username'),
		);
	}

	function getBrowser(){
		$agent=$_SERVER["HTTP_USER_AGENT"];
		//if(strpos($agent,'MSIE')!==false || strpos($agent,'rv:11.0')) //ie11判断
		if(strpos($agent,'MSIE')!==false)
		return "ie";
		else if(strpos($agent,'Firefox')!==false)
		return "firefox";
		else if(strpos($agent,'Chrome')!==false)
		return "chrome";
		else if(strpos($agent,'Opera')!==false)
		return 'opera';
		else if((strpos($agent,'Chrome')==false)&&strpos($agent,'Safari')!==false)
		return 'safari';
		else if(strpos($agent,'rv:11.0'))
		return 'ie11';
		else
		return 'unknown';
	}
?>
