<?php
function getProjects(){
	$Project = D('Project');
	return $Project->getOpenLists();
}

function showMajorName($id, $majors){
	foreach($majors as $major){
		if($major['id'] == $id){
			return $major['name'];
		}
	}
}

function formatDate($time){
	return date('Y-m-d', $time);
}

function isPayed($tag){
	if($tag == 0){
		return '未支付';
	}else{
		return '已支付';
	}
}

function getMajorNames($majors){
	$ms = explode(',', $majors);
	$Major = D('Major');
	foreach($ms as $major){
		echo $Major->getName($major) . ' ';
	}
}

function getExamcenterName($id){
	$Examcenter = D('Examcenter');
	echo $Examcenter->getNameById($id);
}
?>
