<?php
namespace Home\Model;
use Think\Model;
class RuleModel extends Model{
    public function getValue($majors, $examcenterid){
        $info = $this->where(array('examcenterid'=>$examcenterid))->select();
        if(is_null($info))return false;
        foreach($info as $v){
            if($this->arr_equal(explode(',', $majors),explode(',', $v['majorids']))){
                return $v['numofvalue'];
            }
        }
        return false;
    }

    private function arr_equal($arr1, $arr2){
        if(count($arr1)!=count($arr2))return false;
        foreach($arr1 as $v){
            if(!in_array($v, $arr2))return false;
        }
        return true;
    }
}
?>
