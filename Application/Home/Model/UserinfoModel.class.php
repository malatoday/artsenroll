<?php
namespace Home\Model;
use Think\Model;
/**
CREATE TABLE `art_userinfo` (
  `uid` int(8) NOT NULL,
  `province` int(8) NOT NULL,
  `stunumber` varchar(45) NOT NULL,
  `address` varchar(45) NOT NULL,
  `zipcode` int(8) NOT NULL,
  `telofparent` varchar(45) NOT NULL,
  `tel` varchar(20) NOT NULL,
  `sex` int(8) NOT NULL,
  `birthday` varchar(45) DEFAULT NULL,
  `politics` int(8) DEFAULT NULL,
  `isfresh` int(8) DEFAULT NULL,
  `nation` int(8) DEFAULT NULL,
  `school` varchar(45) DEFAULT NULL,
  `qq` varchar(20) DEFAULT NULL,
  `is_science_focus` int(8) DEFAULT '0',
  `grade` varchar(240) DEFAULT '',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `stunumber` (`stunumber`),
  UNIQUE KEY `stunumber_2` (`stunumber`),
  CONSTRAINT `art_userinfo_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `art_user` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

*/
class UserinfoModel extends Model{
	private $_uid;	// 用户id
	private $_province;	//高考省份
	private $_stunumber;	// 学生考号
	private $_address;	// 通讯地址
	private $_zipcode;	// 邮政编码
	private $_telofparent;	// 家长手机
	private $_tel;	// 学生手机
	private $_sex;	// 性别
	private $_birthday;	// 生日
	private $_politics;	// 政治面貌
	private $_isfresh;	// 应往届，0表示往届，1表示应届
	private $_nation;	// 民族
	private $_school;	// 学校
	private $_qq;	// qq号码
	private $_is_science_focus;

	public function __construct(){
		parent::__construct();
		$this->_uid = session('uid');
		$this->initVars();
	}

	public function saveOne($province,
							 $stunumber,
							 $address,
							 $zipcode,
							 $telofparent,
							 $tel,
							 $sex,
							 $birthday,
							 $politics,
							 $isfresh,
							 $nation,
							 $school,
							 $qq,
							 $science){
		$this->_province = $province;
		$this->_stunumber = $stunumber;
		$this->_address = $address;
		$this->_zipcode = $zipcode;
		$this->_telofparent = $telofparent;
		$this->_tel = $tel;
		$this->_sex = $sex;
		$this->_birthday = $birthday;
		$this->_politics = $politics;
		$this->_isfresh = $isfresh;
		$this->_nation = $nation;
		$this->_school = $school;
		$this->_qq = $qq;
		$this->_is_science_focus = $science;
		if($this->isExist()){
			return $this->save($this->convertData());
		}else{
			return $this->add($this->convertData());
		}
	}

	private function convertData(){
		$data = array();
		$data['uid'] = $this->_uid;
		$data['province'] = $this->_province;
		$data['stunumber'] = $this->_stunumber;
		$data['address'] = $this->_address;
		$data['zipcode'] = $this->_zipcode;
		$data['telofparent'] = $this->_telofparent;
		$data['tel'] = $this->_tel;
		$data['sex'] = $this->_sex;
		$data['birthday'] = $this->_birthday;
		$data['politics'] = $this->_politics;
		$data['isfresh'] = $this->_isfresh;
		$data['nation'] = $this->_nation;
		$data['school'] = $this->_school;
		$data['qq'] = $this->_qq;
		$data['is_science_focus'] = $this->_is_science_focus;
		return $data;
	}

	private function isExist(){
		if($this->where(array('uid'=>$this->_uid))->find() === null){
			return false;
		}
		return true;
	}

	private function initVars(){
		$res = $this->where(array('uid'=>$this->_uid))->find();
		if(is_array($res)){
			$this->_province = $res['province'];
			$this->_stunumber = $res['stunumber'];
			$this->_address = $res['address'];
			$this->_zipcode = $res['zipcode'];
			$this->_telofparent = $res['telofparent'];
			$this->_tel = $res['tel'];
			$this->_sex = $res['sex'];
			$this->_birthday = $res['birthday'];
			$this->_politics = $res['politics'];
			$this->_isfresh = $res['isfresh'];
			$this->_nation = $res['nation'];
			$this->_school = $res['school'];
			$this->_qq = $res['qq'];
			$this->_is_science_focus = $res['is_science_focus'];
		}
	}
}
