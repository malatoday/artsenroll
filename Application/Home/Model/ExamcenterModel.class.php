<?php
namespace Home\Model;
use Think\Model;

class ExamcenterModel extends Model{
	public function getExamcenters($province = ''){
		$res = array();
		$list = $this->where(array('inuse'=>1))->select();
		if($province == '')return $list;
		foreach($list as $v){
			if(in_array($province, explode(',', $v['forprovince']))){
				$res[] = $v;
			}
		}
		return $res;
	}

	public function getNames($ids){
		$res = array();
		foreach($ids as $id){
			$res[] = $this->where(array('id'=>$id))->getField('name');
		}
		return $res;
	}

	public function getMajors($examcenter = ''){
		$res = array();
		foreach($examcenter as $v){
			$res = array_merge($res, explode(',', $v['majors']));
		}
		return array_unique($res);
	}

	public function getNameById($id){
		return $this->where(array('id'=>$id))->getField('name');
	}
}

?>
