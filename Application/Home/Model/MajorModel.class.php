<?php
namespace Home\Model;
use Think\Model;
class MajorModel extends Model{
	public function checkClassAndNode($class, $nodes){
		$correct = $this->where(array('pid'=>$class))->select();
		foreach($nodes as $node){
			if(!in_array($node, $correct)){
				return false;
			}
		}
		return true;
	}

	public function getNames($ids){
		$res = array();
		foreach($ids as $id){
			$res[] = $this->where(array('id'=>$id))->getField('name');
		}
		return $res;
	}

	public function getByIds($ids){
		$map['id'] = array('in', implode(',', $ids));
		return $this->where($map)->select();
	}

	public function getName($id){
		return $this->where(array('id'=>$id))->getField('name');
	}
}
