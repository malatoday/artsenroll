<?php
namespace Home\Controller;
use Think\Controller;
class ProjectController extends BaseController{
	public function addProject(){
		$majorclass = I('post.majorclass');
		$majors = I('post.majors');
		$examcenter = I('post.examcenter');
	}

	/**
     * 一个接口，用来被前台ajax调用，返回对应筛选条件的项目列表
	*/
	public function getProjects(){
		$majors = I('post.majors');
		$examcenter = I('post.examcenter');
		$Project = D('Project');
		$data = $Project->getLists(explode(',', $majors), $examcenter);
		$this->ajaxReturn($data);
	}

	/**
     * 此处的project id 默认认为是正确的，不需要再做额外的检查，直接操作数据库即可。
	 * 更加严谨的做法需要在这个地方再次验证一下专业考点是否匹配以及是否合法
	*/
	public function apply(){	// 报名处理
		$majors = I('post.majors');
		$examcenter = I('post.examcenter');
		$uid = session('uid');
		$Enroll = D('Enroll');
		if($Enroll->addOne($uid, $majors, $examcenter) === false){
			$this->error('添加失败，请重试！');
		}
		$this->redirect('Home/Index/pay', array(), 0);
	}

	public function enroll(){
		$majors = I('post.majors');
		$examcenterid = I('post.examcenterid');
		$uid = session('uid');
		$Enroll = D('Enroll');
		$enrollid = $Enroll->addOne($uid, $majors, $examcenterid);
		if($enrollid !== false){
			$data = $Enroll->getInfoByUid($uid);
			$this->ajaxReturn(array('opstatus'=>"success", 'info'=>$data));
		}
		$this->ajaxReturn(array('opstatus'=>"error"));
	}

	public function delete(){
		$enrollid = I('post.enrollid');
		$Enroll = D('Enroll');
		if($Enroll->deleteOne($enrollid) !== false){
			$this->ajaxReturn(array('opstatus'=>'success'));
		}
		$this->ajaxReturn(array('opstatus'=>'error'));
	}

	/**
     * 用户考试报名信息，显示报名状态
	*/
	public function info(){
		$Enroll = D('Enroll');
		$info = $Enroll->getInfoByUid(session('uid'));
		$this->assign('info', $info);
		$this->display();
	}
}
