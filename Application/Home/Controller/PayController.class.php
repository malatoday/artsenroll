<?php
namespace Home\Controller;
use Think\Controller;

class PayController extends BaseController{
    public function testpay(){
        $pay = new \Org\Swupay\Pay(100102 . rand(1, 100), "10.00");
        $pay->setRetrunUrl('http://' . $_SERVER['SERVER_NAME'] . U('Home/Pay/returntest'));
        $pay->setNotifyUrl('http://' . $_SERVER['SERVER_ADDR'] . U('Home/Pay/notifytest'));
        $pay->sign();
        dump($pay->get_query_array());
        $this->assign('query', $pay->get_query_array());
        $this->assign('url', $pay->get_url());
        $this->display();

    }
    public function returntest(){
        echo 'Test success';
    }

    public function notifytest(){
        return 'error';
    }

    public function notifypay(){
        $notify = new \Org\Swupay\Notify('post');
        $info = $notify->checkSign();
        if($info !== false){
            $Enroll = D('Enroll');
            $Rule = D('Rule');
            $Pay = D('Pay');
            $ispayed = $Enroll->checkPayed($info['orderNo']);
            if($ispayed===false){
                $this->error('错误的ID号');
            }else{
                if($ispayed == 1){  // 已经支付过
                    $Pay->saveOne($info);
                    //$this->error('系统错误，请查看您的报名信息');
                    echo 'success';
                }else{
                    $data = $Enroll->where(array('id'=>$info['orderNo']))->find();
                    $amount = $Rule->getValue($data['major'], $data['examcenter']);
                    if($amount != $info['amount']){
                        $this->error('支付金额不匹配，请稍后重试');
                    }else{  // 订单正确无误
                        if($Enroll->setPayed($info['orderNo']) !== false){
                            $Pay->saveOne($info);
                            echo 'success';
                        }else{
                            $this->error('系统错误，请刷新重试');
                        }
                    }
                }
            }
        }else{
            $this->error('支付结果验证失败，请稍后重试');
        }
    }

    public function returnpay(){
        $notify = new \Org\Swupay\Notify('get');
        $info = $notify->checkSign();
        if($info !== false){
            $Enroll = D('Enroll');
            $Rule = D('Rule');
            $Pay = D('Pay');
            $ispayed = $Enroll->checkPayed($info['orderNo']);
            if($ispayed===false){
                $this->error('错误的ID号');
            }else{
                $data = $Enroll->where(array('id'=>$info['orderNo']))->find();
                if($ispayed == 1){  // 已经支付过
                    $Pay->saveOne($info);
                    $this->assign('data', $data);
                    $this->assign('info', $info);
                    $this->display();
                }else{
                    $amount = $Rule->getValue($data['major'], $data['examcenter']);
                    if($amount != $info['amount']){
                        $this->error('支付金额不匹配，请稍后重试');
                    }else{  // 订单正确无误
                        if($Enroll->setPayed($info['orderNo']) !== false){
                            $Pay->saveOne($info);
                            $this->assign('data', $data);
                            $this->assign('info', $info);
                            $this->display();
                        }else{
                            $this->error('系统错误，请刷新重试');
                        }
                    }
                }
            }
        }else{
            $this->error('支付结果验证失败，请稍后重试');
        }
    }

    public function pay(){
        $enrollid = I('get.enrollid');
        $numofvalue = I('get.numofvalue');
        $uid = session('uid');
        $Enroll = D('Enroll');
        $data = $Enroll->where(array('id'=>$enrollid))->find();
        $Rule = D('Rule');
        $res = $Rule->getValue($data['major'], $data['examcenter']);
        if($data === false){
            $this->error('系统错误，请稍后重试');
        }
        if($uid != $data['uid']){
            $this->error('这个支付款项不属于你，请核对后再次尝试或联系工作人员');
        }
        if($data['ispayed'] == 1){
            $this->error('你已经为这个款项支付过了，不必重复支付。');
        }
        if($res != $numofvalue){
            $this->error('系统校验支付金额不匹配，请返回重新尝试');
        }
        $pay = new \Org\Swupay\Pay($enrollid, $numofvalue);
        $pay->setRetrunUrl('http://' . $_SERVER['SERVER_NAME'] . U('Home/Pay/returnpay'));
        $pay->setNotifyUrl('http://' . $_SERVER['SERVER_ADDR'] . U('Home/Notify/notifypay'));
        $pay->setDebug(false);
        $pay->sign();
        $this->assign('query', $pay->get_query_array());
        $this->assign('url', $pay->get_url());
        $this->display();
    }

    public function getValue(){
        $enrollid = I('post.enrollid');
        $uid = session('uid');
        $Enroll = D('Enroll');
        $data = $Enroll->where(array('id'=>$enrollid))->find();
        if($data === false){
            $this->ajaxReturn([
                'opstatus'  =>  'error',
            ]);
        }
        if($uid != $data['uid']){
            $this->ajaxReturn([
                'opstatus'  =>  'error',
                'errorinfo' =>  '这个支付款项不属于你，请核对后再次尝试或联系工作人员',
                'data'  =>  $data
            ]);
        }
        $Rule = D('Rule');
        $res = $Rule->getValue($data['major'], $data['examcenter']);
        if($res!=false){
            $this->ajaxReturn([
                'opstatus'  =>  'success',
                'numofvalue'    =>  $res,
            ]);
        }
        $this->ajaxReturn([
            'opstatus'  =>  'error',
        ]);
    }
}
