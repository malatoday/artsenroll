<?php
namespace Home\Controller;
use Think\Controller;

class NotifyController extends Controller{
    public function notifypay(){
        $notify = new \Org\Swupay\Notify('post');
        $info = $notify->checkSign();
        if($info !== false){
            $Enroll = D('Enroll');
            $Rule = D('Rule');
            $Pay = D('Pay');
            $ispayed = $Enroll->checkPayed($info['orderNo']);
            if($ispayed===false){
                $this->error('错误的ID号');
            }else{
                if($ispayed == 1){  // 已经支付过
                    $Pay->saveOne($info);
                    //$this->error('系统错误，请查看您的报名信息');
                    echo 'success';
                }else{
                    $data = $Enroll->where(array('id'=>$info['orderNo']))->find();
                    $amount = $Rule->getValue($data['major'], $data['examcenter']);
                    if($amount != $info['amount']){
                        $this->error('支付金额不匹配，请稍后重试');
                    }else{  // 订单正确无误
                        if($Enroll->setPayed($info['orderNo']) !== false){
                            $Pay->saveOne($info);
                            echo 'success';
                        }else{
                            $this->error('系统错误，请刷新重试');
                        }
                    }
                }
            }
        }else{
            $this->error('支付结果验证失败，请稍后重试');
        }
    }
}

?>
