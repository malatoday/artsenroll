<?php
namespace Home\Controller;
use Think\Controller;
class PublicController extends Controller{
	public function signin(){
		$this->display();
	}

	public function signup(){
		$this->display();
	}

	public function signinHandle(){
		$User = D('User');
		$username = I('post.username');
		$password = I('post.password');
		if(!empty($username) && !empty($password)){
			$res = $User->checkUser($username, $password);
			if($res === false){
				$this->error('用户名或密码错误，请重试');
			}else{
				session('uid', $res['id']);
				session('username', $username);
				session('rid', $res['role_id']);
				session('name', $res['name']);
				$this->redirect('Home/Index/index', array(), 0);
			}
		}
		$this->error('请输入用户名密码后再次尝试');
	}

	public function signupHandle(){
		$User = D('User');
		$username = I('post.username');
		$reusername = I('post.reusername');
		$password = I('post.password');
		$repassword = I('post.repassword');
		$name = I('post.name');
		if($username == $reusername && $password == $repassword){
			$res = $User->addUser($username, $password, $name);
			if($res === false){
				$this->error('注册失败，请重试', 'signup');
			}else{
				//$this->redirect('Home/Public/signin', array(), 0);
				$this->success('注册成功，正在跳转到登录页面', 'signin');
			}
		}
		$this->error('注册失败，请重试1', 'signin');
	}

	public function logout(){	// 登出操作
		session(null);
		$this->redirect('Home/Public/signin', array(), 0);
	}
}
