<?php
namespace Home\Controller;
use Think\Controller;
class BaseController extends Controller{
	public function __construct(){
		parent::__construct();
		$browser = getBrowser();
		if($browser == 'ie'){
			$this->display('Layout/notsupport');
			exit(0);
		}
		if(isLogin()===false){
			$this->redirect('Home/Public/signin', array(), 0);
		}
	}
}
