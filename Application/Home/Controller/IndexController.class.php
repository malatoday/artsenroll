<?php
namespace Home\Controller;
use Think\Controller;
class IndexController extends BaseController {
    public function index(){    // 首页
        $this->display();
    }

    public function about(){    // 关于我们页面
        $this->display();
    }

    public function info(){ // 完善信息页面
        $User = D('User');
        $Userinfo = D('Userinfo');
        $now = $Userinfo->where(array('uid'=>session('uid')))->find();
        $accountinfo = $User->getInfo(session('uid'));
        $provinces = C('PROVINCE');
        $politicsstatus = C('POLITICS_STATUS');
        $nations = C('NATION');
        $this->assign('now', $now);
        $this->assign('nations', $nations);
        $this->assign('politicsstatus', $politicsstatus);
        $this->assign('provinces', $provinces);
        $this->assign('accountinfo', $accountinfo);
        $this->display();
    }

    public function survey(){   // 志愿调查页面
        $this->display();
    }

    public function grade(){    // 成绩查询
        $this->display();
    }

    public function select(){    // 考点选择
        $Major = D('Major');
        $Examcenter = D('Examcenter');
        $Userinfo = D('Userinfo');
        $Project = D('Project');
        $Enroll = D('Enroll');
        $province = $Userinfo->where(array('uid'=>session('uid')))->getField('province');
        if($province == null){  // 如果省份信息为空
            $this->error('请先完善你的个人信息','info',3);
        }
        $examcenterlist = $Examcenter->getExamcenters($province);
        if(empty($examcenterlist)){
            $this->error('暂时还没有考点支持你所在的省份，请查看招生简章','index',3);
        }
        $majors = $Examcenter->getMajors($examcenterlist);
        $this->assign('enrolllist', $Enroll->getInfoByUid(session('uid')));
        $this->assign('examcenterlist', $examcenterlist);
        $this->assign('majornodes', $Major->getByIds($majors));
        // $this->assign('majorclass', $Major->where(array('level'=>0))->select());
        $this->display();
    }

    public function pay(){    // 在线缴费
        $Enroll = D('Enroll');
        $this->assign('orders', $Enroll->getInfoByUid(session('uid')));
        $this->display();
    }

    public function help(){ // 帮助界面
        $this->display();
    }

    public function profile(){  // 个人信息
        $this->display();
    }

    public function infoHandle(){   // 更新用户信息
        $province = I('post.province');
        $stunumber = I('post.stunumber');
        $restunumber = I('post.restunumber');
        $address = I('post.address');
        $zipcode = I('post.zipcode');
        $telofparent = I('post.telofparent');
        $tel = I('post.tel');
        $sex = I('post.sex');
        $science = I('post.science');
        $birthday = I('post.birthday');
        $politics = I('post.politics');
        $isfresh = I('post.isfresh');
        $nation = I('post.nation');
        $school = I('post.school');
        $qq = I('post.qq');
        $Userinfo = D('Userinfo');
        $res = $Userinfo->saveOne($province,
							 $stunumber,
							 $address,
							 $zipcode,
							 $telofparent,
							 $tel,
							 $sex,
							 $birthday,
							 $politics,
							 $isfresh,
							 $nation,
							 $school,
							 $qq,
                             $science);
        if($res !== false){
            $this->redirect('Home/Index/info', array(), 0);
        }else{
            $this->error('保存失败');
        }
    }

    public function changepasswd(){
        $this->display();
    }

    public function changepasswdHandle(){
        $old = I('post.oldpassword');
        $new = I('post.newpassword');
        $re = I('post.repassword');
        $uid = session('uid');
        $User = D('User');
        $info = $User->getInfo($uid);
        if($info['password'] != $old){
            $this->error('旧密码错误');
        }
        if($new == $re){
            $this->error('两次输入的密码不一致');
        }
        if($User->changePasswd($uid, $new) !== false){
            $this->success('Home/Index/index', '修改成功');
        }
        $this->error('系统错误');
    }
}
