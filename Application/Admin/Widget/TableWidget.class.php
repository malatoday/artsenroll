<?php
namespace Admin\Widget;
use Think\Controller;
class TableWidget extends Controller{
	public function tableDef($head, $body){
		$this->assign('head', $head);
		$this->assign('body', $body);
		$this->display('Table:def');
	}

	public function tableWithFunction($head, $body, $needop = false){
		$this->assign('head', $head);
		$this->assign('body', $body);
		$this->assign('needop', $needop);
		$this->display('Table:withFunction');
	}
}
