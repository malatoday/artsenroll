<?php

function getAccount($id){
    return M('User')->where(array('id'=>$id))->getField('username');
}

function getWeek($num){
    switch($num % 7){
        case 0:
            return '星期日';
        case 1:
            return '星期一';
        case 2:
            return '星期二';
        case 3:
            return '星期三';
        case 4:
            return '星期四';
        case 5:
            return '星期五';
        case 6:
            return '星期六';
        default:
            return 'error';
    }
}

function getScienceFocus($flag){
    if($flag == 0){
        return '文科';
    }
    return '理科';
}

function isAccepted($ip = null){
	if(is_null($ip)){
		return false;
	}else{
		return true;	// 假设IP正确，未来此处应当检测客户端IP地址是否是IP地址池中的IP
	}
}

function getIpById($id){
	$ipcache = S('ips');
	if($ipcache === false){
		$Ip = D('Ip');
		$ips = $Ip->getOpens();
		S('ips', $ips);
		return getIpById($id);
	}else{
		foreach($ipcache as $ip){
			if($ip['id'] == $id){
				return $ip['name'];
			}
		}
	}
}

function getUsernameById($id){
	$usercache = S('users');
	if($usercache === false){
		$User = D('User');
		$users = $User->getAdmins();
		S('users', $users);
		return getUsernameById($id);
	}else{
		foreach($usercache as $user){
			if($user['id'] == $id){
				return $user['username'].'--'.$user['name'];
			}
		}
	}
}

function getRoleById($id){
	$rolecache = S('roles');
	if($rolecache === false){
		$Role = D('Role');
		$roles = $Role->select();
		S('roles', $roles);
		return getRoleById($id);
	}else{
		foreach($rolecache as $role){
			if($role['id'] == $id){
				return $role['name'];
			}
		}
	}
}

function ipcheck($uid, $ip){
	$iptousercache = S('iptousers');
	if($iptousercache === false){
		$Iptouser = D('Iptouser');
		$iptousers = $Iptouser->select();
		S('iptousers', $iptousers);
		return ipcheck($uid, $ip);
	}else{
		foreach($iptousercache as $iptouser){
			if($iptouser['uid'] == $uid && $iptouser['ip']==$ip){
				return 'checked';
			}
		}
	}
}

function accessCheck($rid, $access){
	$Accessrolecache = S('accessroles');
	if($Accessrolecache === false){
		$Accessrole = D('Accessrole');
		$accessroles = $Accessrole->select();
		S('accessroles', $accessroles);
		return accessCheck($rid, $access);
	}else{
		foreach($Accessrolecache as $Accessrole){
			if($Accessrole['role_id']==$rid && $Accessrole['access_id'] == $access){
				return 'checked';
			}
		}
	}
}

function is_superadmin($uid){
	$superadmins = C('superadmin');
	if(in_array($uid, $superadmins)){
		return true;
	}
}

function formatDate($time){
	return date('Y-m-d', $time);
}

function isPayed($tag){
	if($tag == 0){
		return '<span class="text-danger">未支付</span>';
	}else{
		return '<span class="text-success">已支付</span>';
	}
}

function isPayedClear($tag){
    if($tag == 0){
		return '未支付';
	}else{
		return '已支付';
	}
}

function getSex($sex){
	if($sex == 1){
		return '男';
	}else{
		return '女';
	}
}

function getPolitics($id){
	foreach(C('POLITICS_STATUS') as $v){
		if($v['code'] == $id)return $v['name'];
	}
	return '';
}

function getisfresh($id){
	if($id == 0){
		return '往届';
	}
	return '应届';
}

function getNation($id){
	foreach(C('NATION') as $v){
		if($v['code'] == $id){
			return $v['name'];
		}
	}
}

function getMajorNames($majors){
	$ms = explode(',', $majors);
	$Major = D('Major');
	foreach($ms as $major){
		echo '<code>' . $Major->getName($major) . '</code> ';
	}
}

function getMajorNamesClear($majors){
	$ms = explode(',', $majors);
	$Major = D('Major');
	foreach($ms as $major){
		return $Major->getName($major) . ' ';
	}
}

function getExamcenterName($id){
	$Examcenter = D('Examcenter');
	return $Examcenter->getNameById($id);
}

function getUsername($id){
	return M('User')->where(array('id'=>$id))->getField('name');
}

function doNothing($val){
	return $val;
}

function checkInuse($tag){
	trace($tag);
	if($tag == 1){
		echo '<div class="switch" data-on-label="启用中" data-off-label="禁用中"><input name="isopen" type="checkbox" checked /></div>';
	}else{
		echo '<div class="switch" data-on-label="启用中" data-off-label="禁用中"><input name="isopen" type="checkbox" /></div>';
	}
}

function getProvinceName($id){
	foreach(C('PROVINCE') as $var){
		if($var['code'] == $id)return '<kbd>' . $var['name'] . '</kbd>';
	}
}

function getProvinceNameClear($id){
    foreach(C('PROVINCE') as $var){
		if($var['code'] == $id)return $var['name'];
	}
}

function getProvinceNames($ids){
	$res = '';
	foreach(C('PROVINCE') as $var){
		if(in_array($var['code'], explode(',', $ids))){
			$res .= '<kbd>' . $var['name'] . '</kbd> ';
		}
	}
	return $res;
}
