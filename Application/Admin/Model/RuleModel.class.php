<?php
namespace Admin\Model;
use Think\Model;
/**
CREATE TABLE art_rule(
id INT(8) NOT NULL AUTO_INCREMENT PRIMARY KEY,
examcenterid INT(8) NOT NULL,
majorids VARCHAR(45) NOT NULL,
numofvalue FLOAT(6,2) NOT NULL default 0.00,
FOREIGN KEY(examcenterid) REFERENCES art_examcenter(id) ON UPDATE CASCADE ON DELETE RESTRICT
);
 * 每个考点下有多个专业，将该考点下的多个专业进行组合，所有的组合情况都必须分条记录在本表中
*/
class RuleModel extends Model{
    public function saveOne($examid, $majors, $money){
        $data = [
            'examcenterid'  =>  $examid,
            'majorids'  =>  $majors,
            'numofvalue'    =>  $money,
        ];
        $infos = $this->where(array('examcenterid'=>$examid))->select();
        if($infos != null){
            foreach($infos as $v){
                if(explode(',', $v['majorids']) == explode(',', $majors)){
                    $data['id'] = $v['id'];
                    return $this->save($data);
                }
            }
        }
        return $this->add($data);
    }

    public function getByExamid($id){
        return $this->where(array('examcenterid'=>$id))->select();
    }

    /**
     * 根据参数情况求其组合，返回一个数组
    */
    public function combination($arr = array()){
        if(!is_array($arr)){
            return false;
        }
        $len = count($arr);
        $i = 1;
        $res = array();
        while($i <= $len){
            $res[$i] = $this->comb1($arr, $i);
            $i++;
        }
        return $res;
    }

    private function comb1($arr, $len){
        if($len == 1)return $arr;
        $res = array();
        $temp = $this->comb1($arr, $len-1);
        foreach($arr as $v){
            foreach($temp as $vo){
                $tv = (array)$vo;
                if(!in_array($v, $tv)){
                    $tv[] = $v;
                    $tag = true;
                    foreach($res as $value){
                        if($this->arr_equal($value, $tv)){
                            $tag = false;
                        }
                    }
                    if($tag){
                        $res[] = $tv;
                    }

                }
            }
        }
        return $res;
    }

    private function comb($arr, $len){
        $res = array();
        if($len>1){
            foreach($arr as $k=>$v){
                foreach($this->comb($arr, $len-1) as $vo){
                    $vo = (array)$vo;
                    if(!in_array($v, $vo)){
                        $vo[] = $v;
                        $res[] = $vo;
                    }
                    // $temp = array_merge($temp, (array)$vo);
                    // $temp = array_unique($temp);
                    //if(count($temp) == $len){
                    //    $res[] = $temp;
                    //}
                }

            }
        }else if($len == 1){
            $res = $arr;
        }
        return $this->my_arr_unique($res);
    }

    private function my_arr_unique($arr){
        $res = array();
        // while($temp != null){
        //     if(array_search($temp, $res)===false){
        //         $res[] = $temp;
        //     }
        //     next($arr);
        //     $temp = current($arr);
        // }
        foreach($arr as $v){
            if(is_array($v)){
                foreach($res as $vo){
                    if($v != $vo){
                        $res[] = $v;
                    }
                }
            }else{
                if(!in_array($v, $res)){
                    $res[] = $v;
                }
            }
        }
        return $res;
    }

    private function arr_equal($arr1, $arr2){
        if(count($arr1)!=count($arr2))return false;
        foreach($arr1 as $v){
            if(!in_array($v, $arr2))return false;
        }
        return true;
    }
}

?>
