<?php
namespace Admin\Model;
use Think\Model;

class UserinfoModel extends Model{
    public function getInfoById($uid){
        return $this->where(array('uid'=>$uid))->find();
    }
}
