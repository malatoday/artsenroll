<?php 
namespace Admin\Model;
use Think\Model;
/**
 * create table art_logip(
    id INT(8) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    ip VARCHAR(45) NOT NULL,
    occurtime INT(20) NOT NULL, 
    username VARCHAR(45),
    isok INT(1));
*/
class LogipModel extends Model{
	private $_id;	// 日志id
	private $_ip;	// 客户端登陆IP地址
	private $_occurtime;	// 发生的时间
	private $_username;	// 登陆的用户名
	private $_isok;	// 是否登陆成功
	
	public function addLog($username, $isok, $ip){
		$this->_ip = $ip;
		$this->_occurtime = time();
		$this->_username = $username;
		$this->_isok = $isok;
		$this->add($this->combine());
	}
	
	private function combine($needId = false){	// 默认不组装ID值
		if($needId===false){
			return array(
				'ip'	=>	$this->_ip,
				'occurtime'	=>	$this->_occurtime,
				'username'	=>	$this->_username,
				'isok'	=>	$this->_isok
			);
		}else{
			return array(
				'id'	=>	$this->_id,
				'ip'	=>	$this->_ip,
				'occurtime'	=>	$this->_occurtime,
				'username'	=>	$this->_username,
				'isok'	=>	$this->_isok
			);
		}
	}
}