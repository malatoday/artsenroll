<?php
namespace Admin\Model;
use Think\Model;
/**
 * create table art_major(
    -> id INT(8) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    -> name VARCHAR(45) NOT NULL,
	-> level INT(8) NOT NULL default 0,
	-> pid INT(8) NOT NULL default 0);
*/
class MajorModel extends Model{
	private $_id;
	private $_name;
	private $_level;
	private $_pid;
	const L_CLASS = 0;
	const L_NODE = 1;
	public function addMajor($name, $pid = 0, $level = self::L_CLASS){
		if(!empty($name)){
			$data['name'] = $name;
			$data['pid'] = $pid;
			$data['level'] = $level;
			return $this->add($data);
		}
		return false;
	}

	public function getName($id){
		return $this->where(array('id'=>$id))->getField('name');
	}

	public function getClasses(){
		return $this->where(array('level'=>self::L_CLASS))->select();
	}

	public function getNodes(){
		return $this->where(array('level'=>self::L_NODE))->select();
	}
}
