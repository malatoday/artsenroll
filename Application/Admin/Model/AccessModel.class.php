<?php 
namespace Admin\Model;
use Think\Model;
/**
 * create table art_access(
    -> id INT(8) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    -> name VARCHAR(45) NOT NULL,
    -> pid INT(8) NOT NULL default 0,
    -> level INT(8) NOT NULL,
    -> title VARCHAR(45) default null);

*/
class AccessModel extends Model{
	private $_id;	// 唯一标识符
	private $_name;	// 权限名称，如应用名称，控制器名称，方法名称
	private $_pid;	// 当前节点所属父节点ID，应用的父节点为0
	private $_level;	// 当前节点所属层级，1表示应用，2表示控制器，3表示方法
	private $_title;	// 备注信息
	const L_APP = 1;	// APP level
	const L_CONTROLLER = 2;	// Controller level
	const L_ACTION = 3;	// action level
	
	public function addAccess($name, $title = "", $pid = 0, $level = 1){
		$this->_level = $level;
		$this->parseLevel();
		$this->_pid = $pid;
		$this->_title = $title;
		$this->_name = $name;
		if($this->isExist($this->_pid, 'id')){// 名称不存在且pid存在
			$data = array();
			$data['name'] = $this->_name;
			$data['pid'] = $this->_pid;
			$data['level'] = $this->_level;
			$data['title'] = $this->_title;
			return $this->add($data);
		}else{
			return false;
		}
	}
	
	private function isExist($value, $by = 'id'){	// 检查该记录是否存在，参数为以某个字段为标准 
		if($by == 'id' && $value==0)return true;	// 特殊情况，id为0表示根
		if($this->where(array($by=>$value))->find() == null){
			return false;	// 不存在
		}
		return true;
	}
	
	private function parseLevel(){	// 解析用户输入的level是否符合标准，默认为APP，调用之前需要先给成员变量$_level赋值
		switch($this->_level){
			case self::L_APP:
				$this->_level = self::L_APP;
				break;
			case self::L_CONTROLLER:
				$this->_level = self::L_CONTROLLER;
				break;
			case self::L_ACTION:
				$this->_level = self::L_ACTION;
				break;
			default:
				$this->_level = self::L_APP;
				break;
		}
	}
	
	public function getPids(){	// 获取父节点，即level级是L_APP和L_CONTROLLER的节点集合
		$limit = array();
		$limit[] = self::L_APP;
		$limit[] = self::L_CONTROLLER;
		$map = array();
		$map['level'] = array('in', $limit);
		$prepids = $this->where($map)->select();
		$res = array();
		foreach($prepids as $v){
			if($v['pid'] == 0){	// APP节点
				$temp = array();
				$temp[] = $v;
				foreach($prepids as $vo){
					if($vo['pid'] == $v['id']){
						$temp[] = $vo;
					}
				}
				$res[] = $temp;
			}
		}
		return $res;
	}
	
	public function getList(){
		$pre = $this->select();
		$res = array();
		foreach($pre as $v){
			$temp = array();
			if($v['pid'] == 0){	// 是应用
				$temp[] = $v;
				foreach($pre as $vo){
					$tem = array();
					if($vo['pid'] == $v['id']){	// 是控制器
						$tem[] = $vo;
						foreach($pre as $voo){	
							if($voo['pid'] == $vo['id']){	// 是方法
								$tem[] = $voo;
							}
						}
						$temp[] = $tem;
					}
				}
				$res[] = $temp;
			}
		}
		return $res;
	}
	
	public function getLists(){	// getList方法的另一种实现
		$res = array();
		$apps = $this->where(array('level'=>self::L_APP))->select();
		foreach($apps as $app){
			unset($temp);
			$temp = array();
			$temp[] = $app;
			$controllers = $this->where(array('level'=>self::L_CONTROLLER, 'pid'=>$app['id']))->select();
			unset($tem);
			$tem = array();
			foreach($controllers as $controller){
				$actions = $this->where(array('level'=>self::L_ACTION, 'pid'=>$controller['id']))->select();
				$te = array();
				$te[] = $controller;
				$te[] = $actions;
				$tem[] = $te;
				
			}
			$temp[] = $tem;
			
			$res[] = $temp;
		}
		return $res;
	}
}