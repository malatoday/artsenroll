<?php
namespace Admin\Model;
use Think\Model;

class EnrollModel extends Model{
    public function getCountBetween($start, $end, $ispayed = true){
        $map['enrolltime'] = array('between', array($start, $end));
        if($ispayed){
            $map['ispayed'] = array('eq', 1);
        }else {
            $map['ispayed'] = array('eq', 0);   // 未缴费的
        }
        return $this->where($map)->count();
    }

    public function getPayedCountBetween($start, $end){
        $map['ispayed'] = array('eq', 1);
        $map['uptime'] = array('between', array($start, $end));
        return $this->where($map)->count();
    }
}
