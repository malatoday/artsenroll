<?php 
namespace Admin\Model;
use Think\Model;
/**
 * create table art_role(
    -> id INT(8) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    -> name VARCHAR(45) NOT NULL UNIQUE);
*/
class RoleModel extends Model{
	private $_id;
	private $_name;
	public function addRole($name){
		if(!$this->isHasName($name)){
			return false;	// 角色名已经存在
		}else{
			$data = array();
			$data['name'] = $name;
			return $this->add($data);
		}
	}
	
	public function getRoleName($id){
		$res = $this->isExist($id);
		if($res){
			return $res['name'];
		}else{
			return '不存在用户';
		}
	}
	
	private function isHasName($name){
		if($this->where(array('name'=>$name))->find()==null){
			return true;
		}
		return false;
	}
	
	private function isExist($id){
		if($this->where(array('id'=>$id))->find()==null){
			return false;
		}
		return true;
	}
}