<?php 
namespace Admin\Model;
use Think\Model;
/**
 * create table art_project(
    -> id INT(8) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    -> name VARCHAR(45) NOT NULL ,
    -> examcenter VARCHAR(45) NOT NULL default 0,
    -> major VARCHAR(45) NOT NULL default 0,
    -> title VARCHAR(120) NOT NULL default "",
    -> uptime INT(20) NOT NULL,
    -> isopen INT(8) NOT NULL default 0);
*/
class ProjectModel extends Model{
	private $_id;	// 考试项目唯一标识号
	private $_name;	// 考试项目的名称
	private $_examcenter;	// 考试所在考点，可以多个
	private $_major;	// 考试允许的专业，可以多个
	private $_title;	// 关于本场考试的说明
	private $_uptime;	// 最后更新时间
	private $_isopen;	// 考试项目是否开启，如不开启，前台不可见, 0表示不开启，1表示开启
	
	public function addOne($name, $examcenter, $major, $title, $isopen = 0){	// 默认未开启
		$this->_name = $name;
		$this->_title = $title;
		$this->parseExamcenter($examcenter);
		$this->parseMajor($major);
		$this->_isopen = $isopen;
		return $this->add($this->convertData());
	}
	
	private function parseExamcenter($examcenter){	// 将二维数组解析为字符串并保存在成员变量中
		if(!empty($examcenter)){
			$this->_examcenter = implode(',', $examcenter);
		}else{
			$this->_examcenter = '';
		}
	}
	
	private function parseMajor($major){
		if(!empty($major)){
			$this->_major = implode(',', $major);
		}else{
			$this->_major = '';
		}
	}
	
	private function convertData($needid = false){
		$data = array();
		if($needid){
			$data['id'] = $this->_id;
		}
		$data['name'] = $this->_name;
		$data['examcenter'] = $this->_examcenter;
		$data['major'] = $this->_major;
		$data['title'] = $this->_title;
		$data['uptime'] = time();
		$data['isopen'] = $this->_isopen;
		return $data;
	}
}