<?php
namespace Admin\Controller;
use Think\Controller;
class ProjectController extends BaseController{
	public function projectlist(){
		$Project = D('Project');
		$projectlist = $Project->select();
		$this->assign('projectlist', $projectlist);
		$this->display();
	}

	public function addProject(){
		$Examcenter = D('Examcenter');
		$Major = D('Major');
		$examcenterlist = $Examcenter->select();
		$majorlist = $Major->getNodes();
		$this->assign('majorlist', $majorlist);
		$this->assign('examcenterlist', $examcenterlist);
		$this->display();
	}

	public function addProjectHandle(){
		$name = I('post.name');
		$title = I('post.title');
		$examcenter = I('post.examcenter');
		$major = I('post.major');
		$Project = D('Project');
		if($Project->addOne($name, $examcenter, $major, $title, 1) !== false){
			$this->redirect('Admin/Project/projectlist', array(), 0);
		}
		$this->error('系统发送错误，请重试');
	}

	public function updateProject($pro_id=null){
		$pro=D('Project');
		if($pro_id==null){
			$prolist=$pro->select();
			$this->assign("prolist",$prolist);
			$this->display("Project/updatelist");
		}
		else{
			$proinfo=$pro->where(array("id"=>$pro_id))->find();
			if($proinfo==false){
				$this->error("用户名不存在!",U("Admin/User/updateProject"));
			}else{
				$Examcenter = D('Examcenter');
				$Major = D('Major');
				$examcenterlist = $Examcenter->select();
				$majorlist = $Major->getNodes();
				$this->assign('majorlist', $majorlist);
				$this->assign('examcenterlist', $examcenterlist);
				$this->assign("pro",$proinfo);
				$this->assign("pid",$pro_id);
				$this->display();
			}

		}
	}

	public function updateprohandle(){
		$pid=I("post.pid");
		$proname=I("post.name");
		$examcenter=I("post.examcenter");
		$major=I("post.major");
		$title=I("post.title");
		$where['id']=$pid;
		$pro=D("Project");
		$res=$pro->where($where)->find();
		if($res===false){
			$this->error("用户名不存在！",U("Admin/User/updatepro"));
		}
		else{
			$data['id']=$pid;
			$data['name']=$proname;
			$data['examcenter']=$examcenter;
			$data['title']=$title;
			$res=$pro->save($data);
			if ($res===false) {
				$this->error("修改失败，请重试或联系管理员!",U("Admin/User/updateProject"));
			}
			else{
				$this->success("修改成功!",U("Admin/User/updateProject"));
			}
		}
	}

	public function deletepro($pro_id=null){
		if($pro_id==null){
			$this->error('用户信息为空，请重试!',U('Admin/User/updatepro'));
		}
		else{
			$pro=D("User");
			$where['id']=$pro_id;
			$res=$pro->where($where)->delete();
			if($res===false){
				$this->error('删除失败，请重试！',U('Admin/User/updatepro'));
			}
			else{
				$this->success('删除成功！',U('Admin/User/updatepro'));
			}
		}

	}
}
