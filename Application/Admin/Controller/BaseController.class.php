<?php
namespace Admin\Controller;
use Think\Controller;
class BaseController extends Controller{
	private $_uid;
	private $_role_id;
	public function __construct(){
		parent::__construct();
		$browser = getBrowser();
		if($browser == 'ie'){
			$this->show('请不要使用不支持的浏览器，为了您能够正常使用本站，请下载较高版本的chrome或firefox或Edge访问');
			exit(0);
		}
		$this->isLogin();
		$this->checkAuth();
	}

	private function checkAuth(){
		if($this->_role_id == 0){
			$this->redirect('Home/Index/index', array(), 0);
		}
		$Accessrole = D('Accessrole');
		if($Accessrole->hasPermission(CONTROLLER_NAME, 'Controller', $this->_role_id)){
			if($Accessrole->hasPermission(ACTION_NAME, 'Action', $this->_role_id)){
				return true;
			}
		}
		if(is_superadmin($this->_uid)){
			return true;
		}
		$this->error('你没有相关权限，请联系管理员获取');
	}

	private function isLogin(){
		if(session('uid')!=null){
			$this->_uid = session('uid');
			$this->_role_id = session('rid');
			return true;
		}else{
			$this->redirect('Admin/Public/signin', array('Controller'=>CONTROLLER_NAME, 'Action'=>ACTION_NAME), 0);
		}
	}
}
