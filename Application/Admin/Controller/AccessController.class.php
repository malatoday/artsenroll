<?php 
namespace Admin\Controller;
use Think\Controller;
class AccessController extends BaseController{
	public function accesslist(){	// 权限列表
		$Access = D('Access');
		$acclist = $Access->select();
		$this->assign('acclist', $acclist);
		$this->display();
	}
	
	public function addAccess(){	// 新增权限
		$Access = D('Access');
		$nodelist = $Access->getPids();
		$this->assign('nodelist', $nodelist);
		$this->display();
	}
	
	public function allot(){	// 分配权限给角色
		$Role = D('Role');
		$Access = D('Access');
		$Accessrole = D('Accessrole');
		$now = $Accessrole->getList();
		$rolelist = $Role->select();
		$accesslist = $Access->getLists();
		$this->assign('now', $now);
		$this->assign('accesslist', $accesslist);
		$this->assign('rolelist', $rolelist);
		$this->display();
	}
	
	public function addAccessHandle(){	// 新增权限处理函数
		$name = I('post.name');
		$title = I('post.title');
		$pid = I('post.pid');
		$level = I('post.level');
		$Access = D('Access');
		if($Access->addAccess($name, $title, $pid, $level) !== false){
			$this->redirect('Admin/Access/addAccess', array(), 0);
		}else{
			$this->error('请求发生错误，请重试');
		}
	}
	
	public function allotHandle(){
		$rid = I('post.rid');	// 不对role_id进行有效性检查，依赖数据库本身
		$accesses = I('post.accesses');	// 同理
		$Accessrole = D('Accessrole');
		if($Accessrole->allot($rid, $accesses) !== false){
			$this->redirect('Admin/Access/allot', array(), 0);
		}else{
			$this->error('请求发生错误，请重试');
		}
	}

	public function updateAccess($access_id=null){		//更新权限，故居有无access_id判断列表页和具体修改页
		$access=D("Access");
		if($access_id==null){
			$accesses=$access->select();
			$this->assign("accesslist",$accesses);
			$this->display("Access/updatelist");
		}
		else{
			$where['id']=$access_id;
			$res=$access->where($where)->find();
			if($res===false){
				$this->error("权限信息错误，请重试或联系管理员！");
			}
			else{
				$nodelist = $access->getPids();
				$this->assign('nodelist', $nodelist);
				$this->assign("accessinfo",$res);
				$this->display();
			}
		}
	}

	public function updateAccessHandle(){		//更新权限处理
		$aid=I("post.access_id");
		$access=D("Access");
		$where['id']=$aid;
		$res=$access->where($where)->find();
		if($res===false){
			$this->error("权限信息错误，请重试或联系管理员！",U("Admin/Access/updateAccess"));
		}
		else{
			$data['id']=$aid;
			$data['name']=I("post.name");
			$data['pid']=I("post.pid");
			$data['level']=I("post.level");
			$data['title']=I("post.title");
			$res=$access->save($data);
			if($res===false){
				$tihs->error("更新失败,请重试！",U("Admin/Access/updateAccess"));
			}
			else{
				$this->success("更新成功！",U("Admin/Access/updateAccess"));
			}
		}
	}

	public function deleteAccessHandle($access_id=null){		//删除权限处理
		$access=D("Access");
		$where['id']=$access_id;
		$res=$access->where($where)->delete();
		if($res===false){
			$this->error("权限不存在，删除失败！",U("Admin/Access/updateAccess"));
		}
		else{
			$this->success("删除成功!",U("Admin/Access/updateAccess"));
		}
	}
}