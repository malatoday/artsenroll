<?php
namespace Admin\Controller;
use Think\Controller;
class IndexController extends BaseController{
	public function index(){
		$this->display();
	}

	public function log(){
		$Logip = D('Logip');
		$logs = $Logip->select();
		$this->assign('logs', $logs);
		$this->display();
	}

	public function getWeekInfo(){
		$Enroll = D('Enroll');
		$s_now = time();
		$s_today = (date("H")*60 + date("i"))*60 + date("s");
		$s_b_today = time() - $s_today;
		$w_today = date("w");
		$res = array();
		$s_of_day = 24*60*60;
		for($i = 0; $i < 7; $i++){
			$tmp = array();
			$tmp['name'] = getWeek($w_today + $i);
			$start = $s_b_today-$i*$s_of_day;
			$tmp['numofnotpayed'] = $Enroll->getCountBetween($start, $start+$s_of_day, false);
			$tmp['numofpayed'] = $Enroll->getCountBetween($start, $start+$s_of_day, true);
			$res[] = $tmp;
		}
		$this->ajaxReturn($res);
	}

	public function getWeekPayedInfo(){
		$Enroll = D('Enroll');
		$s_now = time();
		$s_today = (date("H")*60 + date("i"))*60 + date("s");
		$s_b_today = time() - $s_today;
		$w_today = date("w");
		$res = array();
		$s_of_day = 24*60*60;
		for($i = 0; $i < 7; $i++){
			$tmp = array();
			$tmp['name'] = getWeek($w_today + $i);
			$start = $s_b_today-$i*$s_of_day;
			$tmp['num'] = $Enroll->getPayedCountBetween($start, $start+$s_of_day);
			$res[] = $tmp;
		}
		$this->ajaxReturn($res);
	}

	public function getExamcenterEnrolls(){
		$Enroll = D('Enroll');
		$Examcenter = D('Examcenter');
		//$examlist = $Examcenter->where(array('inuse'=>1))->select();
        $examlist = $Examcenter->select();
		foreach($examlist as &$v){
			$v['enrollnum'] = $Enroll->where(array('examcenter'=>$v['id']))->count();
			$v['payednum'] = $Enroll->where(array('examcenter'=>$v['id'], 'ispayed'=>1))->count();
		}
		$this->ajaxReturn($examlist);
	}

	public function export(){
		import('Vendor.Phpexcel.PHPExcel');
        $ExcelObj = new \PHPExcel();
        $ExcelObj->getProperties()->setCreator("Parallel")
        							 ->setLastModifiedBy("Parallel")
        							 ->setTitle("Enroll Infomations")
        							 ->setSubject("Enroll Infomations")
        							 ->setDescription("西南大学艺术类网上报名系统报名信息")
        							 ->setKeywords("西南大学 艺术 校考 PHPExcel php")
        							 ->setCategory("统计信息");
        $ExcelObj->setActiveSheetIndex(0)->setCellValue('A1', 'ID')
                                        ->setCellValue('B1', '姓名')
                                        ->setCellValue('C1', '报名时间')
                                        ->setCellValue('D1', '是否已经支付')
                                        ->setCellValue('E1', '报名专业')
                                        ->setCellValue('F1', '考点')
                                        ->setCellValue('G1', '考试费用')
                                        ->setCellValue('H1', '高考省份')
                                        ->setCellValue('I1', '考生号')
                                        ->setCellValue('J1', '通讯地址')
                                        ->setCellValue('K1', '邮政编码')
                                        ->setCellValue('L1', '家长电话')
                                        ->setCellValue('M1', '本人电话')
                                        ->setCellValue('N1', '性别')
                                        ->setCellValue('O1', '生日')
                                        ->setCellValue('P1', '政治面貌')
                                        ->setCellValue('Q1', '应往届')
                                        ->setCellValue('R1', '文理科')
                                        ->setCellValue('S1', '民族')
                                        ->setCellValue('T1', '所在学校')
                                        ->setCellValue('U1', '本人qq')
                                        ->setCellValue('V1', '身份证号');
		$id = I('get.id');
        $Enroll = D('Enroll');
        $Major = D('Major');
        $Pay = D('Pay');
        $Userinfo = D('Userinfo');
        $data = $Enroll->where(array('examcenter'=>$id))->select();
        $i = 2;
        foreach($data as $people){
            $info = $Userinfo->where(array('uid'=>$people['uid']))->find();
            $ExcelObj->setActiveSheetIndex(0)->setCellValue('A' . $i, $people['id'])
                                            ->setCellValue('B' . $i, getUsername($people['uid']))
                                            ->setCellValue('C' . $i, formatDate($people['enrolltime']))
                                            ->setCellValue('D' . $i, isPayedClear($people['ispayed']))
                                            ->setCellValue('E' . $i, getMajorNamesClear($people['major']))
                                            ->setCellValue('F' . $i, getExamcenterName($people['examcenter']))
                                            ->setCellValue('G' . $i, $Pay->getValue($people['id']))
                                            ->setCellValue('H' . $i, getProvinceNameClear($info['province']))
                                            ->setCellValue('I' . $i, '\'' . $info['stunumber'])
                                            ->setCellValue('J' . $i, $info['address'])
                                            ->setCellValue('K' . $i, '\'' . $info['zipcode'])
                                            ->setCellValue('L' . $i, $info['telofparent'])
                                            ->setCellValue('M' . $i, '\'' . $info['tel'])
                                            ->setCellValue('N' . $i, getSex($info['sex']))
                                            ->setCellValue('O' . $i, $info['birthday'])
                                            ->setCellValue('P' . $i, getPolitics($info['politics']))
                                            ->setCellValue('Q' . $i, getisfresh($info['isfresh']))
                                            ->setCellValue('R' . $i, getScienceFocus($info['is_science_focus']))
                                            ->setCellValue('S' . $i, getNation($info['nation']))
                                            ->setCellValue('T' . $i, $info['school'])
                                            ->setCellValue('U' . $i, '\'' . $info['qq'])
                                            ->setCellValue('V' . $i, '\'' . getAccount($people['uid']));
            $i++;
        }
        $ExcelObj->setActiveSheetIndex(0);
        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="01simple.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0
        $objWriter = \PHPExcel_IOFactory::createWriter($ExcelObj, 'Excel2007');
        $objWriter->save('php://output');
        exit();
        
	}
}