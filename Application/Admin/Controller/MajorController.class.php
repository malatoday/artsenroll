<?php
namespace Admin\Controller;
use Think\Controller;
class MajorController extends BaseController{
	public function majorlist(){	// 专业列表
		$Major = D('Major');
		$majorlist = $Major->select();
		$this->assign('majorlist', $majorlist);
		$this->display();
	}

	public function addmajor(){	// 新增专业
		$Major = D('Major');
		$this->assign('majorclass', $Major->getClasses());
		$this->display();
	}

	public function addmajorHandle(){
		$name = I('post.name');
		$pid = I('post.pid');
		$level = I('post.level');
		$Major = D('Major');
		if($Major->addMajor($name, $pid, $level)!==false){
			$this->redirect('Admin/Major/majorlist', array(), 0);
		}
		$this->error('操作失败，请重新尝试');
	}
}
