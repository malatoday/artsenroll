<?php 
namespace Admin\Controller;
use Think\Controller;
class PublicController extends Controller{
	public function signin(){	// 登陆表单
		$this->display();
	}
	
	public function signinHandle(){
		$ip = $_SERVER['REMOTE_ADDR'];
		$username = I('post.username');
		$password = I('post.password');
		$Logip = D('Logip');
		if(isAccepted($ip)){	// 检测IP地址是否是可访问IP
			$User = D('User');
			$res = $User->checkUser($username, $password);
			if($res===false){
				$Logip->addLog($username, false, $ip);
				$this->error('用户名或密码错误！');
			}else{
				$Logip->addLog($username, true, $ip);	// 登陆成功
				session('uid', $res['id']);
				session('rid', $res['role_id']);
				$this->redirect('Admin/Index/index', array(), 0);
			}
		}else{
			// 记录尝试登陆日志，向客户端返回错误
			$Logip->addLog($username, false, $ip);
			$this->error('你不具有管理员权限，请联系相关人员解决！');
		}
	}
	
	public function signout(){	// 登出操作
		session(null);
		$this->redirect('Admin/Public/signin', array(), 0);
	}
}