<?php
namespace Admin\Controller;
use Think\Controller;
class ExamcenterController extends BaseController{
	public function examcenterlist(){
		$Examcenter = D('Examcenter');
		$examcenterlist = $Examcenter->select();
		$this->assign('examcenterlist', $examcenterlist);
		$this->display();
	}

	public function addexamcenter(){
		$provincelist = C('PROVINCE');
		$Major = D('Major');
		$this->assign('provincelist', $provincelist);
		$this->assign('majorlist', $Major->getNodes());
		$this->display();
	}

	public function addexamcenterHandle(){
		$name = I('post.name');
		$from = I('post.province');
		$for = I('post.allow');
		$major = I('post.major');
		$starttime = I('post.examstart');
		$endtime = I('post.examend');
		$Examcenter = D('Examcenter');
		if($Examcenter->addOne($name, $from, $for, $major,$starttime,$endtime, 1)){
			$this->redirect('Admin/Examcenter/examcenterlist', array(), 0);
		}
		$this->error('系统错误，请重试');
	}

	public function editHandle(){
		$id = I('post.id');
		$name = I('post.name');
		$from = I('post.province');
		$for = I('post.allow');
		$major = I('post.major');
		$starttime = I('post.examstart');
		$endtime = I('post.examend');
		$Examcenter = D('Examcenter');
		if($Examcenter->saveOne($id ,$name, $from, $for, $major,$starttime,$endtime, 1)!==false){
			$this->redirect('Admin/Examcenter/examcenterlist', array(), 0);
		}
		$this->error('系统错误，请重试');
	}

	public function setStatus(){
		$id = I('post.id');
		$status = I('post.status')=='true' ? 1 : 0;
		$Examcenter = D('Examcenter');
		if($Examcenter->setOpen($id, $status)!==false){
			$this->ajaxReturn(array('opstatus'=>'success','opparams'=>$_POST));
		}else{
			$this->ajaxReturn(array('opstatus'=>'error','opparams'=>$_POST));
		}
	}

	public function edit(){
		$id = I('get.id');
		$Examcenter = D('Examcenter');
		$provincelist = C('PROVINCE');
		$Major = D('Major');
		$this->assign('provincelist', $provincelist);
		$this->assign('majorlist', $Major->getNodes());
		$info = $Examcenter->getOne($id);
		$this->assign('info', $info);
		$this->display();
	}

	public function rule(){
		$id = I('get.id');
		$Examcenter = D('Examcenter');
		$Rule = D('Rule');
		$info = $Examcenter->getOne($id);
		$rules = $Rule->combination(explode(',', $info['majors']));
		$hasexistrules = $Rule->getByExamid($id);
		$this->assign('hasexistrules', $hasexistrules);
		$this->assign('examcenterid', $id);
		$this->assign('rule', $rules);
		$this->display();
	}

	public function saverule(){
		$examid = I('post.examcenterid');
		$majors = I('post.majors');
		$money = I('post.money');
		$Rule = D('Rule');
		$Examcenter = D('Examcenter');
		$info = $Examcenter->getOne($examid);
		if(explode(',', $majors) != (array)array_intersect(explode(',', $majors), explode(',', $info['majors']))){
			$this->ajaxReturn([
				'opstatus'	=>	'操作失败，考点专业不匹配',
				'majors'	=>	$majors,
				'exammajors'	=>	$info,
				'examid'	=>	$examid,
				'jj'	=>	array_intersect(explode(',', $info['majors']), explode(',', $majors)),
				'gx'	=>	explode(',', $majors)
			]);
		}
		if($Rule->saveOne($examid, $majors, $money) !== false){
			$this->ajaxReturn([
				'opstatue'	=>	'success'
			]);
		}
		$this->ajaxReturn([
			'opstatue'	=>	'系统错误'
		]);
	}
}
