<?php 
namespace Admin\Controller;
use Think\Controller;
class RoleController extends BaseController{
	public function rolelist(){	// 角色列表
		$Role = D('Role');
		$rolelist = $Role->select();
		$this->assign('rolelist', $rolelist);
		$this->display();
	}
	
	public function addrole(){	// 新增角色
		$this->display();
	}
	
	public function addroleHandle(){	// 新增角色处理
		$name = I('post.name');
		$Role = D('Role');
		if($Role->addRole($name)!==false){
			$this->redirect('Admin/Role/addrole', array(), 0);
		}
		$this->error('角色名称已经存在，请不要重复添加');
	}

	public function updateRole($role_id=null){		//更新角色，根据有无role_id值判断是列表操作还是具体修改操作
		$role=D("Role");	
		if($role_id==null){
			$roles=$role->select();
			$this->assign("rolelist",$roles);
			$this->display("Role/updatelist");
		}
		else{
			$where['id']=$role_id;
			$res=$role->where($where)->find();
			if($res===false){
				$this->error("角色参数错误，请重试或联系管理员！",U("Admin/Role/updateRole"));
			}
			else{
				$this->assign("roleinfo",$res);
				$this->display();
			}
		}
	}

	public function updateRoleHandle(){		//更新角色处理
		$rid=I("post.rid");
		$name=I("post.name");
		$where['id']=$rid;
		$role=D("Role");
		$res=$role->where($where)->find();
		if($res===false){	//自行检测数据真实性
			$this->error("角色不存在，请重试或联系管理员！",U("Admin/Role/updateRole"));
		}
		else{
			$res=$role->where(array("name"=>$name))->find();
			if($res==false){
				$data['id']=$rid;
				$data['name']=$name;
				$res=$role->save($data);
				if($res===false){
					$this->error("修改失败,请重试!",U("Admin/Role/updateRole"));
				}
				else{
					$this->success("修改成功!",U("Admin/Role/updateRole"));
				}
			}
			else{
				$this->error("此角色名已存在，请重新输入",U("Admin/Role/updateRole",array("role_id"=>$rid)));
			}
			
		}
	}

	public function deleteRoleHandle($role_id){		//删除角色处理
		$role=D("Role");
		$where['id']=$role_id;
		$res=$role->where($where)->find();
		if($res===false){
			$this->error("角色信息错误，请重试或联系管理员！",U("Admin/Role/updateRole"));
		}
		else{
			$res=$role->where($where)->delete();
			if($res===false){
				$this->error("删除失败，请重试!",U("Admin/Role/updateRole"));
			}
			else{
				$this->success("删除成功！",U("Admin/Role/updateRole"));
			}
		}
	}
}