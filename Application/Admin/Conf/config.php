<?php
return array(
	'superadmin'	=>	array(
		//'测试用户'	=>	5,	// 以此形式新增超级用户，超级用户对后台操作不受权限控制，需要谨慎添加
        
		'admin'		=>  1,
	),
	'OP_TYPE'	=>	array(	// 表格操作类型
		'OP_DELETE'	=>	1,	// 删除		00001
		'OP_EDIT'	=>	2,	// 编辑		00010
		'OP_RESET'	=>	4,	// 重置密码 	100
		'OP_DEFINE_RULE'	=>	8
	),
	'RESET_PASSWD'	=>	'123456',	// 重置密码
);