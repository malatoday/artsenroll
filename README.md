# artsenroll

> 本项目为西南大学招生就业处艺术类报名网站

## 概述

本项基于开源框架ThinkPHP开发，使用了Bootstrap3框架，还有startbootstrap-sb-admin模板。在此作为开发者衷心地感谢他们。

项目演示站地址为：[http://art.malatoday.com](http://art.malatoday.com)

其中：

前台地址：[前台](http://art.malatoday.com/index.php/Home/Index/index)

后台地址：[后台](http://art.malatoday.com/index.php/Admin/Index/index)

前台演示账号未创建，请自行创建。

后台演示账号如下：

- 账号：admin
- 密码：abc123

## 完成情况

### 已完成

- 用户注册和登陆
- 完善用户信息
- 前台除缴费和成绩查询外的其他静态页面
- 用户权限管理
- 管理员登陆日志
- 管理员登陆IP管理（测试账号无视IP无视权限，拥有最高权限）
- 考点专业管理
- 考试项目管理
- 前台考点专业选择
- 前台项目报名

### 未完成

- 以上所有管理的编辑功能
- 以上所有管理的删除功能
- 前台志愿调查
- 后台管理员个人信息管理（查看个人信息和修改密码）
- 导入导出用户数据
- 学生报名信息管理
- 支付接入
- 成绩管理（导入导出，编辑删除，查询）

## 版权

- 本软件开源仅为方便招就处老师查阅，所有版权都归开发者和招就处所有，未经过允许不得用于其他地方。
